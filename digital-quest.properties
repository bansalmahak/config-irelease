# Node environment
NODE_ENV=production
#------------------------------------------------------------------------------
######## E. ENTITLEMENTS CONFIGURATION ######## ::: SECTION START
#
#### E1. Entitlement interface :::: Optional ####
# Possible values: mock | active
# Default: active
# mock: Reads entitlement profile from mock_entl.js in <projectRoot/src> folder
# active: Connects to Entitlement Service
AUTH_MODE=active
#### E2. System user entitlement :::: Required ####
# Possible values: true | false
# Default: false
# true: Indicates that the component is to be considered a system user in entitlements (eg. Gazetteer)
# false: End user entitlements apply
#
SYSTEM_USER_AUTH=true
######## E. ENTITLEMENTS CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## Q. QUERY STORE CONFIGURATION ######## ::: SECTION START
#
#### Q1. Query store parameters used in development/test :::: Optional ####
# Possible values: true | false
# Default: true
# The query store is used as a convenience to load a query/variable set in Graphiql
#
LOAD_QUERY_STORE=false
QUEST_PROFILER=true
# Only load root level schema dependencies
SCHEMA_BUILD_SCOPE = baseOnly
#### Q2. Default query set to be loaded :::: Optional ####
# If not provided no query will be loaded
#
DEFAULT_QUERY_LOAD=viewer
######## Q. QUERY STORE CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## P. PROJECTION STORE CONFIGURATION ######## ::: SECTION START
#
#### P1. CF Service Name for Elastisearch cluster :::: Optional ####
# Default value is elasticsearch
# Provide a value if you wish to override
# This is a Quest instance level configuration and will apply to all
# component packakges which will retrieve their configurations
#
# CF_SVC_NAME_ELASTICSEARCH=elasticsearch
#### P2. CF Service Name for specific package :::: Optional ####
# This is only applicable at an individual component package level
# The specific package will bind to the service name specified
# When provided in the Quest configuration serves as an override
#
# Overrides can be provided with a suffix
# ENTL - EntitlementsService
# ACCT - AccountServices
# ORG - OrgService
# FXRATE - FxRatesService
# BUSCAL - BusinessCalendarService
# REFDATA - ReferenceDataService
# LIQI - LiquidityICLService
# LIQS - LiquiditySweepsService
# LIQC - LiquidityCommonService
# PAY - PaymentsService
# CNR - CNRService
#
# Examples
# CF_SVC_NAME_ELASTICSEARCH_ENTL=entl-els-projection
# CF_SVC_NAME_ELASTICSEARCH_ORG=org-els-projection
#### P3. ES Connection string :::: Required ####
# This is only used in local development.
# Is not considered when deployed in CF
#
ES_CONNSTRING_QUEST=http://localhost:9200
#### P4. ES Server Partition for indexes :::: Optional ####
# Default value: N/A (empty)
# Indexes will be created/accessed with this prefix
# e.g. if Partition has a value of "DIT" then the index for Account Services will be "dit.acct"
#
ES_PARTITION=quest
#### P5. Response size (limit) for Search queries :::: Optional ####
# Default value: 2000
# Search queries will throttle the number of records retrieved based on this value
#
ES_QUERY_LIMIT=10000
#### P6. Response size (limit) for Aggregate queries :::: Optional ####
# Default value: 50
# Aggregate queries will throttle the number of buckets retrieved based on this value
#
ES_AGG_QUERY_LIMIT=1000
#### P6. Timeout threshold in ms :::: Optional ####
# Default value: 30000 ms
#
ES_TIMEOUT=30000
######## P. PROJECTION STORE CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## E. REGISTRY CONFIGURATION ######## ::: SECTION START
#
#### E1. Enable discovery :::: Optional ###
# Possible values: true | false
# Default value: true
#
EUREKA_DISCOVERY_ENABLED=true
#### E2. Eureka discovery name :::: Required ####
# Default value: digital-quest
# If there are multiple quest contexts supported this must be provided
#
EUREKA_DISCOVERY_NAME=digital-quest
#### E3. Eureka service name :::: Optional ####
# Default value: digital-registry
# Eureka service name to connect for discovery services
#
# CF_SVC_NAME_DIGITAL_REGISTRY=digital-registry
#### E4. Eureka registration method :::: Required ####
# Possible values: direct | route
# Default value: route
# Eureka registration supporting
# :: direct: Direct container access registering IP
# :: route: Route is registered
#
EUREKA_REGISTRATION_METHOD=route
#### E5. Eureka Registry Server URI :::: Optional ####
# Eureka registry server URI e.g. http://<server route>/eureka/
#
# SVC_URI_REGISTRY=https://EurekaUser:Pa55w0rd!23@digital-registry-cbx.cfapps.io/eureka/
#### E6. Eureka Request Maximum Retry :::: Optional ####
# Number of times to retry all requests to eureka
# Default value: 3
EUREKA_MAX_RETRIES=10

#### E7. Eureka Request Retry Delay :::: Optional ####
# milliseconds to wait between retries. This will be multiplied by the # of failed retries.
# Default value: 500
EUREKA_REQ_RETRY_DELAY=10000

######## E. REGISTRY CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## X. RABBITMQ CONFIGURATION ######## ::: SECTION START
#
#### X1. RabbitMQ Server URI :::: Required ###
#
# SVC_URI_RABBITMQ=
# RMQ_URI=amqp://dituser:dituser@ec2-52-211-125-22.eu-west-1.compute.amazonaws.com:5672/dittest
#### X2. RabbitMQ service name :::: Required ####
# Default value: rabbitmq
# RabbitMQ service name to connect for event publishing
#
# CF_SVC_NAME_RABBITMQ=rabbitmq
#### X3. Exchange/DLE/Q/DLQ/Key/DLK Info :::: Required ####
# All values except the exchange will be within the control
# of the base application
# Allow configuration - this will permit setup of exchange/qs/bindings
RMQ_ALLOW_CONFIG=true
# Exchange
RMQ_EXCHANGE=cbxevents
# DL Exchange
RMQ_DLE=
# Queue
RMQ_QUEUE=
# Routing Key
RMQ_KEY=
# DLQ
RMQ_DLQ=
# DLK
RMQ_DLKEY=
# Prefetch
RMQ_PREFETCH=25
# Consumer Tag
RMQ_TAG=quest

MQ_ENABLED=false

#### X4. Application instance region :::: Required ####
#
APP_INSTANCE_REGION=Asia
#### X5. Application instance country :::: Required ####
#
APP_INSTANCE_COUNTRY=Thailand
######## X. RABBITMQ CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## L. LOG CONFIGURATION ######## ::: SECTION START
#### L1. Logging level :::: Optional ####
# Supported values are
# error - Only errors are logged
# info - Information logs are enabled
# debug - Debug info is included in the logs
# Default value: info
#
LOG_LEVEL=debug
#### L2. Console logging :::: Optional ####
# Directed to standard output
#
CONSOLE_LOG=true
#### L3. Logs directed to file :::: Required ####
# Directed to file
# Default location is /logs/quest.log
#
FILE_LOG=false
#### L4. File name for File Logs :::: Optional ####
# Will be available in logs folder
#
# FILE_LOG_NAME=quest.log
######## L. LOG CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## R. RESPONSE PARAMETER CONFIGURATION ######## ::: SECTION START
#
#### R1. Include error stack trace :::: Optional ####
# Possible values: true | false
# Default value: false
# Set to true if you require stack traces to be included in logs/ errors section in response
#
ERROR_STACK_TRACE=false
#### R2. Include error path :::: Optional ####
#
ERROR_PATH=true
#### R3. Include timer information in meta :::: Optional ####
# NOTE: To be made optional with default value as false
#
TIMER=true
######## R. RESPONSE PARAMETER CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## S. SECURITY CONFIGURATION ######## ::: SECTION START
#### S1. User claims location :::: Optional ####
# Possible values: bearer | header
# Default value: bearer
# :: bearer: bearer id_token in Authorization header
# :: header: custom igtb_headers - igtb_user/igtb_domain
#
USER_CLAIMS_LOCATION=bearer
#### S2. Validate User token :::: Optional ####
# Possible values: true | false
# Default value: true
# Verify signature and expiry of token
# NOTE: Must be able to toggle both of the above independently
#
USER_TOKEN_VALIDATION=true
#### S3. Verify Shared Secret :::: Required ####
# Possible values: true | false
# Verify shared secret for system inquiries (within the delivery tier)
#
VERIFY_SHARED_SECRET=true
#### S3A. Shared Secret :::: Required ####
# Shared secret configuration if S3. is set to true
#
SHARED_SECRET=6c3d57fe-cb79-46d0-945d-c2f8e30120c8
#### S4. Keystore configuration parameters :::: Required ####
#
KEYSTORE_KID_IDP=NDFBOTFCQjA3RDMyNDhCM0IyREY3RTJGN0IwMkU4RTk5RDlDODEzNw
######## S. SECURITY CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## M. MISCELLANEOUS CONFIGURATION ######## ::: SECTION START
#### M1. Dependant packages :::: Required ####
# NOTE: This will be phased out
#
DEP_PKGS=true
#### M2. GraphIql enabled :::: Optional ####
# NOTE: This is for development/test usage
#
GRAPHIQL_ENABLED=true
#### M3. GraphQL Server Port :::: Optional ####
# Optional configuration to control port on which graphql requests are received
# Specific value; If 'CF' then irrespective of local/cloud appEnv.port will be used
#
SERVER_PORT=

# max accepted request body for each request in mb
REQUEST_BODY_SIZE=50mb

#### M4. Quest route prefix used in GraphIql :::: Optional ####
# Possible value: quest
# Default value: N/A (empty)
# Required to support Graphiql route in Gatekeeper
#
QUEST_ROUTE_PREFIX=
#### M6. NewRelic APM Configuration :::: Optional ####
# NEW_RELIC_ENABLED=false
# NEW_RELIC_LICENSE_KEY=9a0343bb1dd3aecf0b2da2e92302b503098cec38
#### M7. Development only :::: Optional ####
# If token validation is not required then custom headers can be provided
# for user identity transmission in Request
#
# DEV_USER=DHartm390
# DEV_DOMAIN=premierGroup
DEV_USER=
DEV_DOMAIN=
#### M8. Codacy project token :::: Optional ####
# Required for development
# Integration with Codacy
#
#CODACY_PROJECT_TOKEN=9892413b4c5d4ee38f5adf8b5f574446
#### M9. Greeting on Server Start :::: Optional ####
#GREETING=Welcome
######## M. MISCELLANEOUS CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## N. SUBCRIPTIO[N]S CONFIGURATION ######## ::: SECTION START
#### N1. Subscriptions enabled ? :::: Required ####
# Possible values: true | false
# Default value: true
#
SUBSCRIPTIONS_ENABLED=true
#### N2. Subscription port :::: Optional ####
# Possible values: <Port #> e.g. 4443
# If not specified this will be the same as the App Port
# In CF this is 4443 by default
# In OpenShift - 8000 for ws and 8443 for wss
#
SUBSCRIPTION_PORT=80

#### N3. Redis Enabled :::: Required ####
# Possible values: true/false (default: false)
#
REDIS_ENABLED=true

#### N4. Redis Parameters :::: Required ####
# Possible values:
# SVC_HOST_REDIS: <Host name/address> If not specified - localhost
# SVC_PWD_REDIS: <Password> If not specified - null
# SVC_PORT_REDIS: <Port #> If not specified - 6379
#
# SVC_HOST_REDIS=
# SVC_PWD_REDIS=
# SVC_PORT_REDIS=

#### N5. Redis scope :::: Required ####
# For consuming application this will usually be either subscriber or publisher
# For Quest this is expected to be subscriber
# Possible values: subscriber/publisher/all
# Default value: subscriber
#
REDIS_SCOPE=subscriber

####N6. Other options
REDIS_CONN_RETRIES=25
REDIS_CONN_NAME=quest
REDIS_KEY_PREFIX=
# Connection retry delay in ms
REDIS_RETRY_DELAY=30000
######## N. SUBCRIPTIO[N]S CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## C. CONFIG CONFIGURATION ######## ::: SECTION START
#### C1. Quest Private Key :::: Required ####
# Used if Loading of externalised configurations is false
igtbPrivateKeys.entl = -----BEGIN PRIVATE KEY-----MIIBUwIBADANBgkqhkiG9w0BAQEFAASCAT0wggE5AgEAAkEAwzEq4yDRATlxqxPZITo/Tmhqv3FvsEtfB/n1ia3ZxFBdKPSvWW9A4FhlkmCAOZyuqkSpGXWB4BWpMHhH+a60BwIDAQABAkBLkdSV3NWuUKwmXOfaim+KDrkNZ4CjU3r2Xprmc+10qC9fToTGHnXDPRuoolJfVGtTo9XEJ0AewCnu7PX+oWIpAiEA4gwn8XJHCwc22TjloXqUUOjHrKwDYNxdfqWKG1cjvCsCIQDdDlpOaG5X6cpBVkL5cktX9Cp9kA1xhLNEEXG2JfwNlQIgVpBDJ5IUpKOBnJPOVBVGAOnztSs2K/yXjS1FgwzOsXsCIGez6QeplEJn6jt2lVrilJgBcsGPB89+en7vVBIohbN1AiAeZkMkkvUC8HGLiJUAW7OCpb4ioRX7PqschgHDgWVl5Q==-----END PRIVATE KEY-----
#### C2. Quest Certificate :::: Required ####
# Used if Loading of externalised configurations is false
igtbCertificates.entl = -----BEGIN CERTIFICATE-----MIIBtjCCAWCgAwIBAgIEXC8IITANBgkqhkiG9w0BAQsFADBiMQswCQYDVQQGEwJJTjELMAkGA1UECAwCVE4xDDAKBgNVBAcMA0NITjESMBAGA1UECgwJSW50ZWxsZWN0MRUwEwYDVQQLDAxpZ3RiLWNieC5jb20xDTALBgNVBAMMBGVudGwwHhcNMTkwMTA0MDcxNTQ1WhcNMjAwMTA0MDcxNTQ1WjBiMQswCQYDVQQGEwJJTjELMAkGA1UECAwCVE4xDDAKBgNVBAcMA0NITjESMBAGA1UECgwJSW50ZWxsZWN0MRUwEwYDVQQLDAxpZ3RiLWNieC5jb20xDTALBgNVBAMMBGVudGwwXDANBgkqhkiG9w0BAQEFAANLADBIAkEAwzEq4yDRATlxqxPZITo/Tmhqv3FvsEtfB/n1ia3ZxFBdKPSvWW9A4FhlkmCAOZyuqkSpGXWB4BWpMHhH+a60BwIDAQABMA0GCSqGSIb3DQEBCwUAA0EAGOWSOKRYV5kMGQIfBfkfkFBVJNCw+PoippWPWqyDE4cW4GmtVjnnLBAaCvdyqk0a1I3NOs5RHo8yPaVvK9407w==-----END CERTIFICATE-----
#### C3. IDP Certificate :::: Required ####
# Used if Loading of externalised configurations is false
igtbCertificates.NDFBOTFCQjA3RDMyNDhCM0IyREY3RTJGN0IwMkU4RTk5RDlDODEzNw = -----BEGIN CERTIFICATE-----MIIC/zCCAeegAwIBAgIJOqpL6Sw/5g0pMA0GCSqGSIb3DQEBCwUAMB0xGzAZBgNVBAMTEmlndGItY2J4LmF1dGgwLmNvbTAeFw0xNzEyMDUwNjEwMDlaFw0zMTA4MTQwNjEwMDlaMB0xGzAZBgNVBAMTEmlndGItY2J4LmF1dGgwLmNvbTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMhNIs8y/ylb25wdtyTub0IaagKUbMiLvOeQrXqf5PN0fTSzDItRHbqlWUDv+U4ajzJhND9zyDBefY3ZCxcvC6gIR4blixDtoebjoLW3GxgIdM5DOWxgRFinbZdkpCUgHdvjlXgsPPHQttiTqqRhPFJjHHKaLepqMsD/TAO0KOBLy/UXncKlp1lD6j9ASM+1nycdvitDW31zaUvPFWTFQjnUF84Y9yqhtOJtOHOiZGE2Vz4wLRbGrNYqTcTMZf+R6TqlqgmqOCx9eblqJCjOMfYo+tU0uyLW+1CIexFOT9IUum1KnCZQvMOOrYWbAqb1HCtUAi8s4toYk2T/P+zfJwsCAwEAAaNCMEAwDwYDVR0TAQH/BAUwAwEB/zAdBgNVHQ4EFgQUITR+sINi+t7sX4aYeJhQJRjNjO8wDgYDVR0PAQH/BAQDAgKEMA0GCSqGSIb3DQEBCwUAA4IBAQCO6L0TLFrH2QaUtc0cKrLFPwGhqLmIoyP3cYOjjT0ytASEssWzjPJobGNNC5AuRo96tNeC+t4Keh6YhcBrUbuZXQDa2OCM75xY4D/lY3VyXBDAicMZhHLc/urD+LTyWcIljFN6ZIYEgnwkpADzVDSiju+gCn3mVoDvidl8Bk2xNi411GDh20CeF49bz0mrYxXxwKqVtEx6LK2Mam/+4hKR7bgrrReeF06P1XNwMO/YZsZPE2jori7UdhhvDOWivdE2kbGGuvRh7IzY7hs7lOyE57T4z35O29JLABF0E9k/CER2YjvfrDwNf6ttUUIyU/T3AR4I7LuMSMQ2oVa3OzUu-----END CERTIFICATE-----
#### C4. KID External Certificate :::: Required ####
# Used if Loading of externalised configurations is false
igtbCertificates.library-default-v1 = -----BEGIN CERTIFICATE-----MIIBDzCBuqADAgECAgRaMZAeMA0GCSqGSIb3DQEBCwUAMA8xDTALBgNVBAoMBGlndGIwHhcNMTcxMjEzMjAzOTU4WhcNMjcxMjEzMjAzOTU4WjAPMQ0wCwYDVQQKDARpZ3RiMFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBALWYSsXPFjWAu1vb/6bo1Xd3+fOOQoyLQ2r4fZlwKCLFuToYgA9tvb+/egZBHf2eMhYOLuKni49eF0zeBRSA/mcCAwEAATANBgkqhkiG9w0BAQsFAANBADh+bEQb0V821KIc4svhj6rZnbklrsEdi2jlcfkvUHbKohUILhA3JNXfJ7Ss6FJDznfVbnQ09+SLyZ/3KyVeQU4=-----END CERTIFICATE-----
# igtbCertificates.library-default-v1 = -----BEGIN CERTIFICATE-----\nMIIBDzCBuqADAgECAgRaMZAeMA0GCSqGSIb3DQEBCwUAMA8xDTALBgNVBAoMBGlndGIwHhcNMTcxMjEzMjAzOTU4WhcNMjcxMjEzMjAzOTU4WjAPMQ0wCwYDVQQKDARpZ3RiMFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBALWYSsXPFjWAu1vb/6bo1Xd3+fOOQoyLQ2r4fZlwKCLFuToYgA9tvb+/egZBHf2eMhYOLuKni49eF0zeBRSA/mcCAwEAATANBgkqhkiG9w0BAQsFAANBADh+bEQb0V821KIc4svhj6rZnbklrsEdi2jlcfkvUHbKohUILhA3JNXfJ7Ss6FJDznfVbnQ09+SLyZ/3KyVeQU4=\n-----END CERTIFICATE-----
# igtbCertificates.library-default-v1 = -----BEGIN CERTIFICATE-----MIIBDzCBuqADAgECAgRaMZAeMA0GCSqGSIb3DQEBCwUAMA8xDTALBgNVBAoMBGlndGIwHhcNMTcxMjEzMjAzOTU4WhcNMjcxMjEzMjAzOTU4WjAPMQ0wCwYDVQQKDARpZ3RiMFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBALWYSsXPFjWAu1vb/6bo1Xd3+fOOQoyLQ2r4fZlwKCLFuToYgA9tvb+/egZBHf2eMhYOLuKni49eF0zeBRSA/mcCAwEAATANBgkqhkiG9w0BAQsFAANBADh+bEQb0V821KIc4svhj6rZnbklrsEdi2jlcfkvUHbKohUILhA3JNXfJ7Ss6FJDznfVbnQ09+SLyZ/3KyVeQU4=-----END CERTIFICATE-----

######## C. CONFIG CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## I. APPLICATION EXTERNAL INTERFACE CONFIGURATION  ######## ::: SECTION START
#### I1. BBL Counter FX Rate Interface API :::: Required ####
# Possible values: mock api | interface api
# Default value: mock api
#
FX_CONTRACT_API=https://igtb-cbx-mock-rest-api-snapshot-v01.cfapps.io/app/fxContracts
#### I2. BBL Today's Deals Core Interface API :::: Required ####
# Possible values: mock api | interface api
# Default value: mock api
#
TODAYS_DEALS_API=http://appserver01.irelease.igtb.digital:20184/MH/RESTWS/ServicePost
# TODAYS_DEALS_API=http://appserver01.irelease.igtb.digital:20184/MH/RESTWS/ServicePost
#### I3. BBL Forward Contract Core Interface API :::: Required ####
# Possible values: mock api | interface api
# Default value: mock api
#
FORWARD_CONTRACT_API=http://appserver01.irelease.igtb.digital:20184/MH/RESTWS/ServicePost
# FORWARD_CONTRACT_API=http://appserver01.irelease.igtb.digital:20184/MH/RESTWS/ServicePost
#### I4. BBL Loan Rates Interface API :::: Required ####
# Possible values: mock api | interface api
# Default value: mock api
# 
BBL_GET_LOANRATES=http://appserver01.irelease.igtb.digital:20184/MH/RESTWS/ServicePost
#### I5. BBL Deposit Rates Interface API :::: Required ####
# Possible values: mock api | interface api
# Default value: mock api
# 
BBL_GET_DEPOSITRATES=http://appserver01.irelease.igtb.digital:20184/MH/RESTWS/ServicePost
# BBL Header Key and values for Loan and Deposit Interface API
BBL_HEADER_KEY=ocp-apim-subscription-key
BBL_HEADER_VALUE=16cc08ff010d461dba593ebd4d153224
#### I6. BBL Contact Bank Account Validation API :::: Required ####
# Possible values: mock api | interface api
# Default value: mock api
# 
BANK_ACCT_VALIDATE_API=http://appserver01.irelease.igtb.digital:20184/MH/RESTWS/ServicePost
#### I7. BBL ServiceProvide or Consumer Account Validation API :::: Required ####
# Possible values: mock api | interface api
# Default value: mock api
# 
CONSUMER_ACCT_VALIDATE_API=http://igtb-cbx-mock-rest-api-snapshot-v01.cfapps.io/app/consumerAccountValidation

#### I8. BBL Contract Exchange amount Interface API :::: Required ####
# Possible values: mock api | interface api
# Default value: mock api
#
FX_CONTRACT_FILTER_API=http://appserver01.irelease.igtb.digital:20184/MH/RESTWS/ServicePost
# FX_CONTRACT_FILTER_API=http://appserver01.irelease.igtb.digital:20184/MH/RESTWS/ServicePost

#### I9. BBL/CS Compute Exchange FxRate REST API Interface :::: Required ####
# Possible values: mock api | interface api
# Default value: mock api
#
# FX_COMPUTE_INTERFACE_API=http://webserver.irelease.igtb.digital:10001/dtbcommon/services/v1/common/computefx
FX_COMPUTE_INTERFACE_API=http://webserver.irelease.igtb.digital:10001/dtbcommon/services/v1/common/computefx

# BBL Accounts Mutual funds retrieve info
BBL_GET_ACCOUNTS_MUTUALFUND_DATA=http://appserver01.irelease.igtb.digital:20184/MH/RESTWS/ServicePost
# BBL Accounts fetch cheque Image
BBL_GET_ACCOUNTS_CHEQUEIMAGE=http://igtb-cbx-mock-rest-api-snapshot-v01.cfapps.io/app/accountsChequeImage
# BBL Accounts fetch Prompt Pay Transactions info
BBL_GET_ACCOUNTS_PROMPTPAY_DATA=http://appserver01.irelease.igtb.digital:20184/MH/RESTWS/ServicePost
# BBL Accounts fetch Cash Digi Transactions info
BBL_GET_ACCOUNTS_CASHDIGI_DATA=http://igtb-cbx-mock-rest-api-snapshot-v01.cfapps.io/app/accountsCashDigi
# Payments server url
IPSH_SERVER_URl=http://appserver01.irelease.igtb.digital:20124/ipshstp/paymentsAPI/v1/inquiry/

# PaymentRails for Payments Option Transfer 
ENV_TRANSFER=['BTRD0','TTRD0']

# cutoff time will be combination of CUTOFF_TIME_HOUR and CUTOFF_TIME_MINUTES
# EOD cuttoff  time in 24 hour format
CUTOFF_TIME_HOURS=8
# EOD cuttoff  minutes 
CUTOFF_TIME_MINUTES= 58
# how many minutes prior the pre cutoff script needs to be executed
PRE_CUTOFF_TIME_MINUTES= 3


#######################################################
#Client ID of the digital gatekeeper in eureka registry
#######################################################
DIGITAL_GATEKEEPER_EUREKAID=digital-gatekeeper

# Action-Api Context Path
AVAILABLE_LIMITS_CONTEXT_PATH=/limits/igtb-limits/v1/availablelimits
SERVICE_ELEMENTS_LIMITS_CONTEXT_PATH=/limits/igtb-limits/v1/availableLimitsForServiceElements

GATEKEEPER_URL=http://digital-gatekeeper-internal-service-cbx.apps.irelease-openshift.us-east-1.igtb.digital

APP_CLOUD_WORKFLOW_HISTORY_URL=${GATEKEEPER_URL}/approvalwf/igtb-approvalwf/v1/viewWorkflowHistory
APP_CLOUD_WORKFLOW_DETAILS_URL=${GATEKEEPER_URL}/approvalwf/igtb-approvalwf/v1/viewWorkflowDetails
APP_CLOUD_LATEST_ACTION_URL=${GATEKEEPER_URL}/approvalwf/igtb-approvalwf/v1/viewLatestAction
APP_CLOUD_PENDING_ACTION_URL=${GATEKEEPER_URL}/approvalwf/igtb-approvalwf/v1/viewPendingActions
APP_CLOUD_WORKFLOW_RULES_URL=${GATEKEEPER_URL}/approvalwf/igtb-approvalwf/v1/viewWorkflowRule
APP_CLOUD_WORKFLOW_TYPE_URL=${GATEKEEPER_URL}/approvalwf/igtb-approvalwf/v1/viewWorkflowType


######## I. APPLICATION EXTERNAL INTERFACE CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## AE. APPLICATION ENV CONFIGURATION  ######## ::: SECTION START
# BBL FX Rate Base Currency as per implementation bank 
BASE_CURRENCY=THB
# Fx Rate All target currency supported by defualt 
FX_ALL_CURRENCY=CAD,GBP,INR,JPY,SGD,USD,THB
# Default reference currency
DEFAULT_REF_CURRENCY=THB
######## AE. APPLICATION ENV CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------

########  DATA LOADER CACHE CONFIGURATION ######## ::: SECTION START
#------------------------------------------------------------------------------

# Cache Variables
# Unit: Second

# Organisation
CACHE_EXPIRY_PERIOD_BANK=86400 
CACHE_EXPIRY_PERIOD_CONTACT=900
CACHE_EXPIRY_PERIOD_CONTACTACCOUNT=900
CACHE_EXPIRY_PERIOD_CONTACTLASTACTIVITY=900
CACHE_EXPIRY_PERIOD_CORPORATE=900
CACHE_EXPIRY_PERIOD_DOMAIN=900
CACHE_EXPIRY_PERIOD_HOSTBANKCOMMUNICATIONINFO=86400
CACHE_EXPIRY_PERIOD_ORGANISATION=900
CACHE_EXPIRY_PERIOD_RECENTSEARCH=120
CACHE_EXPIRY_PERIOD_USER=300
CACHE_EXPIRY_PERIOD_USERPREFERENCE=300

# Reference Data
CACHE_EXPIRY_PERIOD_AMOUNTFORMAT=86400
CACHE_EXPIRY_PERIOD_CITY=43200
CACHE_EXPIRY_PERIOD_CONTINENT=86400
CACHE_EXPIRY_PERIOD_COUNTRY=43200
CACHE_EXPIRY_PERIOD_CURRENCY=86400
CACHE_EXPIRY_PERIOD_DATEFORMAT=86400
CACHE_EXPIRY_PERIOD_FILESIZEOPTION=86400
CACHE_EXPIRY_PERIOD_HOST=86400
CACHE_EXPIRY_PERIOD_LOCALE=86400
CACHE_EXPIRY_PERIOD_STATE=43200
CACHE_EXPIRY_PERIOD_SUBCONTINENT=86400
#CACHE_EXPIRY_PERIOD_TIMEFORMAT=86400
CACHE_EXPIRY_PERIOD_TIMEZONE=86400

# Accounts
CACHE_EXPIRY_PERIOD_BALANCE=60

########  DATA LOADER CACHE CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
KAFKA_ENABLED=false

QUEST_URI=digital-quest-service-cbx.apps.irelease-openshift.us-east-1.igtb.digital
SERVER_PROTOCOL=http

# FX-rate currency conversion scale
CURRENCY_CONVERSION_DECIMALS=4

### Host Country code
HOST_COUTNRY_CODE=TH

### Host Bank(Organisation) Key
HOST_ORG_KEY=BKKB

# if its true means we have to use acct_base otherwise acct
ROUTING_ENABLED=true

# PDF export font size options
ACCOUNTS_EXPORT_FONT_RECORDS=12
ACCOUNTS_EXPORT_FONT_HEADER=11

#-----------------------------------------------------------------------------------------

# Retrieving the sweep products
LIQ_SWEEP_PRODUCTS=BBLDOMESTICSINGLECCY,CROSSBANKSWEEP,RESTRICTNEW

# Kafka for Liquidity Account alias message push
SVC_URI_KAFKA=elb-kafka-7e79bad516be639a.elb.us-east-1.amazonaws.com:9095
# if its true means we have to use acct_base otherwise acct
ROUTING_ENABLED=true


