# Node environment
NODE_ENV=production
#------------------------------------------------------------------------------
######## E. ENTITLEMENTS CONFIGURATION ######## ::: SECTION START
#
#### E1. Entitlement interface :::: Optional ####
# Possible values: mock | active
# Default: active
# mock: Reads entitlement profile from mock_entl.js in <projectRoot/src> folder
# active: Connects to Entitlement Service
#
AUTH_MODE=active

######## E. ENTITLEMENTS CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## Q. QUERY STORE CONFIGURATION ######## ::: SECTION START
#
#### Q1. Query store parameters used in development/test :::: Optional ####
# Possible values: true | false
# Default: true
# The query store is used as a convenience to load a query/variable set in Graphiql
#
# LOAD_QUERY_STORE=true

#### Q2. Default query set to be loaded :::: Optional ####
# If not provided no query will be loaded
#
DEFAULT_QUERY_LOAD=accountBalances

######## Q. QUERY STORE CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## A. APPLICATION CONFIGURATION ######## ::: SECTION START
#### IMPORTANT: This must correspond to the schema definition ####
#
# A1. Application name used in Quest GraphQL Schema as Interface name :::: Required ####
APP_NAME_ACCT=AccountServices
APP_NAME_ACCT_BASE=AccountServices

#### A2. Application code for resolver package ::::â€‚â€‚Required ####
# The projection store index name must correspond to this code (with lower casing applied)
APP_CODE_ACCT=ACCT
APP_CODE_ACCT_EOD=ACCT_EOD
APP_CODE_ACCT_BASE=ACCT_BASE

#### A3. Application Connector for resolver package ::::â€‚â€‚Required ####
# The projection store connector option to be used
APP_CONNECTOR_ACCT=elasticSearch
APP_CONNECTOR_ACCT_EOD=elasticSearch
APP_CONNECTOR_ACCT_BASE=elasticSearch

#### A4. Application Connector Protocol for resolver package ::::â€‚â€‚Required ####
# The projection store connector protocol option to be used
# NOTE: This will be removed eventually
APP_CONN_PROTOCOL_ACCT=http
APP_CONN_PROTOCOL_ACCT_EOD=http
APP_CONN_PROTOCOL_ACCT_BASE=http

######## A. APPLICATION CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## P. PROJECTION STORE CONFIGURATION ######## ::: SECTION START
#
#### P1. CF Service Name for Elastisearch cluster :::: Optional ####
# Default value is elasticsearch
# Provide a value if you wish to override
# This is a Quest instance level configuration and will apply to all
# component packakges which will retrieve their configurations
#
# CF_SVC_NAME_ELASTICSEARCH=elasticsearch

#### P2. CF Service Name for specific package :::: Optional ####
# This is only applicable at an individual component package level
# The specific package will bind to the service name specified
# When provided in the Quest configuration serves as an override
#
# Overrides can be provided with a suffix
# ENTL - EntitlementsService
# ACCT - AccountServices
# ORG - OrgService
# FXRATE - FxRatesService
# BUSCAL - BusinessCalendarService
# REFDATA - ReferenceDataService
# LIQI - LiquidityICLService
# LIQS - LiquiditySweepsService
# LIQC - LiquidityCommonService
# PAY - PaymentsService
# CNR - CNRService
#
# Examples
# CF_SVC_NAME_ELASTICSEARCH_ENTL=entl-els-projection
# CF_SVC_NAME_ELASTICSEARCH_ORG=org-els-projection

#### P3. ES Connection string :::: Required ####
# This is only used in local development.
# Is not considered when deployed in CF
#
ES_CONNSTRING_ACCT=http://localhost:9200
ES_CONNSTRING_ACCT_EOD=http://localhost:9200
ES_CONNSTRING_ACCT_BASE=http://localhost:9200
#ES_CONNSTRING_ACCT=http://single-elastic-node-lb-2087575466.eu-west-1.elb.amazonaws.com
#ES_CONNSTRING_ORG=http://single-elastic-node-lb-2087575466.eu-west-1.elb.amazonaws.com
#ES_CONNSTRING_ENTL=http://single-elastic-node-lb-2087575466.eu-west-1.elb.amazonaws.com
#ES_CONNSTRING_BUSCAL=http://single-elastic-node-lb-2087575466.eu-west-1.elb.amazonaws.com
#ES_CONNSTRING_FXRATE=http://single-elastic-node-lb-2087575466.eu-west-1.elb.amazonaws.com
#ES_CONNSTRING_REFDATA=http://single-elastic-node-lb-2087575466.eu-west-1.elb.amazonaws.com

#### P4. ES Server Partition for indexes :::: Optional ####
# Default value: N/A (empty)
# Indexes will be created/accessed with this prefix
# e.g. if Partition has a value of "DIT" then the index for Account Services will be "dit.acct"
#
ES_PARTITION=quest

#### P5. Response size (limit) for Search queries :::: Optional ####
# Default value: 2000
# Search queries will throttle the number of records retrieved based on this value
#
# ES_QUERY_LIMIT=2000

#### P6. Response size (limit) for Aggregate queries :::: Optional ####
# Default value: 50
# Aggregate queries will throttle the number of buckets retrieved based on this value
#
# ES_AGG_QUERY_LIMIT=50

#### P6. Timeout threshold in ms :::: Optional ####
# Default value: 30000 ms
#
# ES_TIMEOUT=30000

######## P. PROJECTION STORE CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## E. REGISTRY CONFIGURATION ######## ::: SECTION START
#
#### E1. Enable discovery :::: Optional ###
# Possible values: true | false
# Default value: true
#
# EUREKA_DISCOVERY_ENABLED=true

#### E2. Eureka discovery name :::: Required ####
# Default value: digital-quest
# If there are multiple quest contexts supported this must be provided
#
EUREKA_DISCOVERY_NAME=ingestion-account-balances

#### E3. Eureka service name :::: Optional ####
# Default value: digital-registry
# Eureka service name to connect for discovery services
#
# CF_SVC_NAME_DIGITAL_REGISTRY=digital-registry

#### E4. Eureka registration method :::: Optional ####
# Possible values: direct | route
# Default value: route
# Eureka registration supporting
# :: direct: Direct container access registering IP
# :: route: Route is registered
#
EUREKA_REGISTRATION_METHOD=route

#### E7. Eureka Ports :::: Optional ####
# Port/Secure Port numbers for Route registration method
# Defaults:
# CF_APP_PORT_EUREKA=80
# CF_SECURE_APP_PORT_EUREKA=443
# EUREKA_ENABLE_SECURE_PORT=false

######## E. REGISTRY CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## L. LOG CONFIGURATION ######## ::: SECTION START

#### L1. Logging level :::: Optional ####
# Supported values are
# error - Only errors are logged
# info - Information logs are enabled
# debug - Debug info is included in the logs
# Default value: info
#
LOG_LEVEL=debug

#### L2. Console logging :::: Optional ####
# Directed to standard output
#
CONSOLE_LOG=true

#### L3. Logs directed to file :::: Required ####
# Directed to file
# Default location is /logs/quest.log
#
FILE_LOG=false

#### L4. File name for File Logs :::: Optional ####
# Will be available in logs folder
#
# FILE_LOG_NAME=quest.log

######## L. LOG CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## R. RESPONSE PARAMETER CONFIGURATION ######## ::: SECTION START
#
#### R1. Include error stack trace :::: Optional ####
# Possible values: true | false
# Default value: false
# Set to true if you require stack traces to be included in logs/ errors section in response
#
# ERROR_STACK_TRACE=false

#### R2. Include error path :::: Optional ####
#
# ERROR_PATH=false

#### R3. Include timer information in meta :::: Optional ####
# NOTE: To be made optional with default value as false
#
# TIMER=true

######## R. RESPONSE PARAMETER CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## S. SECURITY CONFIGURATION ######## ::: SECTION START

#### S1. User claims location :::: Optional ####
# Possible values: bearer | header
# Default value: bearer
# :: bearer: bearer id_token in Authorization header
# :: header: custom igtb_headers - igtb_user/igtb_domain
#
USER_CLAIMS_LOCATION=bearer

#### S2. Validate User token :::: Optional ####
# Possible values: true | false
# Default value: true
# Verify signature and expiry of token
# NOTE: Must be able to toggle both of the above independently
#
USER_TOKEN_VALIDATION=false

######## S. SECURITY CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## M. MISCELLANEOUS CONFIGURATION ######## ::: SECTION START

#### M1. Dependant packages :::: Optional ####
# NOTE: This will be phased out
#
# DEP_PKGS=true

#### M2. GraphIql enabled :::: Optional ####
# NOTE: This is for development/test usage
#
GRAPHIQL_ENABLED=true

#### M3. Quest route prefix used in GraphIql :::: Optional ####
# Possible value: /quest
# Default value: N/A (empty)
# Required to support Graphiql route in Gatekeeper
#
# QUEST_ROUTE_PREFIX=

#### M4. Apollo engine configuration :::: Optional ####
# APOLLO_ENGINE=false
# APOLLO_ENGINE_KEY=service:prasvenk007-DigitalQuest:SGRBE4lyBnr4yU4R09W-HA
# APOLLO_TRACING=false
# APOLLO_CACHE_CONTROL=false

#### M5. NewRelic APM Configuration :::: Optional ####
# NEW_RELIC_LICENSE_KEY=9a0343bb1dd3aecf0b2da2e92302b503098cec38

#### M6. Development only :::: Optional ####
# If token validation is not required then custom headers can be provided
# for user identity transmission in Request
#
# DEV_USER=DHartm390
# DEV_DOMAIN=premierGroup

DEV_USER=
DEV_DOMAIN=

#### M7. Codacy project token :::: Optional ####
# Required for development
# Integration with Codacy
#
CODACY_PROJECT_TOKEN=9892413b4c5d4ee38f5adf8b5f574446

#### M8. Greeting on Server Start :::: Optional ####
# GREETING=Welcome

######## M. MISCELLANEOUS CONFIGURATION ######## ::: SECTION START
#------------------------------------------------------------------------------
# If component type is ingestion then additional ingestion related bootstrapping is required
QUEST_PACKAGE_TYPE=ingestion
# Kafka service URI
# SVC_URI_KAFKA=localhost:9092
SVC_URI_KAFKA=elb-kafka-7e79bad516be639a.elb.us-east-1.amazonaws.com:9095
# Consumer Group Id - account-balances
KAFKA_CONSUMER_GROUPID=account-balances-group
# Kafka Topic for consumption - ACCOUNT-BALANCES-INGESTION
KAFKA_SUBSCRIPTION_TOPIC=ACCOUNT-BALANCES-INBOUND-TOPIC
# Kafka DLQ Topic - For quarantining messages that failed processing
KAFKA_DLQ_TOPIC=ACCOUNT-BALANCES-DLQ-TOPIC
# Connection time out
KAFKA_CONN_TIMEOUT=30000
# Heartbeat Interval
KAFKA_HEARTBEAT_INTERVAL=3000
# Number of conn retries
KAFKA_CONN_RETRIES=5
KAFKA_ENABLED=true
# Ingestion Model
INGESTION_MODEL=AccountBalanceFeed
# message Context
KAFKA_MESG_CONTEXT=account-balances-ingestion
# BBL Accounts fetch Prompt Pay Transactions info
BBL_GET_ACCOUNTS_PROMPTPAY_DATA=https://igtb-cbx-mock-rest-api-snapshot-v01.cfapps.io/app/accountsCashPromptPayData
# BBL Accounts fetch Cash Digi Transactions info - Api not yet received
BBL_GET_ACCOUNTS_CASHDIGI_DATA=https://igtb-cbx-mock-rest-api-snapshot-v01.cfapps.io/app/accountsCashDigi
BBL_GET_ACCOUNTS_CASHDIGI_METHOD=GET

MQ_ENABLED=true
# RMQ_DL_QUEUE=dlq.ingestion-accountbalance
RMQ_DLQ=dlq.ingestion-accountbalance
RMQ_SUBSCRIPTION_QUEUE=ingestion-accountbalance-outgoing
REQUEST_BODY_SIZE=50mb

#SVC_URI_RABBITMQ=amqp://cbx-app:s3cr3t@elb-rabbitmq-26a15669ddb01173.elb.us-east-1.amazonaws.com:5675

APPEND_SEQUENCE_VERSION=true

# SUBSCRIPTIONS enabled
SUBSCRIPTIONS_ENABLED=false
ES_DIRECT_USE_SVC_URI=true
ES_SSL_ENABLED=false
KAFKA_SSL_ENABLED=false
RMQ_PREFETCH=1
RMQ_KEY=accountservices
RMQ_EXCHANGE=accountservices
RMQ_DLE=accountservices
RMQ_DLKEY=ingestion-accountbalance.dlk
ES_CONNSTRING_ACCT_BASE=http://localhost:9200

# index to ingest intraday balances and transactions
INTRA_DAY_INDEX=acct_intraday
# index which contains accounts data and to ingest funds and secondary accounts
BASE_INDEX=acct_base
# single index if ROUTING_ENABLED false
SINGLE_INDEX=acct
# index to ingest EOD balances and transactions
EOD_INDEX=acct_eod
# whether to have single or multi index logic false : single true: multi index
ROUTING_ENABLED=false

####### JWT Certificates
igtbCertificates.library-default-v1 = -----BEGIN CERTIFICATE-----\nMIIBDzCBuqADAgECAgRaMZAeMA0GCSqGSIb3DQEBCwUAMA8xDTALBgNVBAoMBGlndGIwHhcNMTcxMjEzMjAzOTU4WhcNMjcxMjEzMjAzOTU4WjAPMQ0wCwYDVQQKDARpZ3RiMFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBALWYSsXPFjWAu1vb/6bo1Xd3+fOOQoyLQ2r4fZlwKCLFuToYgA9tvb+/egZBHf2eMhYOLuKni49eF0zeBRSA/mcCAwEAATANBgkqhkiG9w0BAQsFAANBADh+bEQb0V821KIc4svhj6rZnbklrsEdi2jlcfkvUHbKohUILhA3JNXfJ7Ss6FJDznfVbnQ09+SLyZ/3KyVeQU4=\n-----END CERTIFICATE-----

# Allow configuration - this will permit setup of exchange/qs/bindings
RMQ_ALLOW_CONFIG=true